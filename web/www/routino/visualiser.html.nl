<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="openstreetmap routino verifier">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=no">

<title>Routino : Visualisering van routeringsgegevens</title>

<!--
Routino data visualiser web page.

Part of the Routino routing software.

This file Copyright 2008-2017 Andrew M. Bishop

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
-->

<!-- Page elements -->
<script src="page-elements.js" type="text/javascript"></script>
<link href="page-elements.css" type="text/css" rel="stylesheet">

<!-- Router and visualiser shared features -->
<link href="maplayout.css" type="text/css" rel="stylesheet">

<!-- Visualiser specific features -->
<script src="mapprops.js" type="text/javascript"></script>
<link href="visualiser.css" type="text/css" rel="stylesheet">

<!-- Map parameters -->
<script src="mapprops.js" type="text/javascript"></script>

<!-- Map loader -->
<script src="maploader.js" type="text/javascript"></script>

</head>
<body onload="map_load('map_init();');">

<!-- Left hand side of window - data panel -->

<div class="left_panel">

<div class="tab_box">
<span id="tab_visualiser" onclick="tab_select('visualiser');" class="tab_selected" title="Select data options">Visualiser</span>
<span id="tab_router" onclick="tab_select('router');" class="tab_unselected" title="Plan a route">Router</span>
<span id="tab_data" onclick="tab_select('data');" class="tab_unselected" title="View database information">Data</span>
</div>

<div class="tab_content" id="tab_visualiser_div">

<div class="hideshow_box">
<span class="hideshow_title">Routino Visualisering</span>
Op deze webpagina kunnen de gegevens gevisualiseerd worden, die door Routino worden gebruikt bij de routeplanning. Alleen de gegevens die relevant zijn voor de routeberekening worden getoond, en sommige informatie wordt dus uitgesloten
<div class="center">
<a target="other" href="http://www.routino.org/">Routino Website</a>
|
<a target="other" href="documentation/">Documentatie</a>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_language_show" onclick="hideshow_show('language');" class="hideshow_show">+</span>
<span id="hideshow_language_hide" onclick="hideshow_hide('language');" class="hideshow_hide">-</span>
<span class="hideshow_title">Taal</span>

<div id="hideshow_language_div" style="display: none;">
<table>
<tr>
<td><a id="lang_en_url" href="visualiser.html.en" title="English language webpage">English</a>
<td>(EN)
<tr>
<td><a id="lang_cs_url" href="visualiser.html.cs" title="Stránka v češtině">Česky</a>
<td>(CS)
<tr>
<td><a id="lang_de_url" href="visualiser.html.de" title="Deutsche Webseite">Deutsch</a>
<td>(DE)
<tr>
<td><a id="lang_es_url" href="visualiser.html.es" title="Español webpage">Español</a>
<td>(ES)
<tr>
<td><a id="lang_fi_url" href="visualiser.html.fi" title="Suomi webpage">Suomi</a>
<td>(FI)
<tr>
<td><a id="lang_fr_url" href="visualiser.html.fr" title="Page web en français">Français</a>
<td>(FR)
<tr>
<td><a id="lang_hu_url" href="visualiser.html.hu" title="Magyar weboldal">Magyar</a>
<td>(HU)
<tr>
<td><a id="lang_it_url" href="visualiser.html.it" title="Italiano webpage">Italiano</a>
<td>(IT)
<tr>
<td><a id="lang_nl_url" href="visualiser.html.nl" title="Nederlandse web pagina">Nederlands</a>
<td>(NL)
<tr>
<td><a id="lang_pl_url" href="visualiser.html.pl" title="Polski webpage">Polski</a>
<td>(PL)
<tr>
<td><a id="lang_ru_url" href="visualiser.html.ru" title="Страница на русском языке">Русский</a>
<td>(RU)
<tr>
<td><a id="lang_sk_url" href="visualiser.html.sk" title="Stránka v slovenčine">Slovenčina</a>
<td>(SK)
</table>
<a target="translation" href="http://www.routino.org/translations/">Routino Translations</a>
</div>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Instructies</span>
Zoom in en gebruik dan de knoppen om de gegevens te downloaden. De server zal alleen gegevens aanleveren als het geselecteerde gebied klein genoeg is
</div>

<div class="hideshow_box">
<span class="hideshow_title">Status</span>
<div id="result_status">
<div id="result_status_no_data">
<b><i>Geen data getoond</i></b>
</div>
<div id="result_status_data" style="display: none;">
</div>
<div id="result_status_failed" style="display: none;">
<b>Geen data gevonden om te tonen</b>
</div>
<div id="result_status_junctions" style="display: none;">
<b># splitsingen behandeld</b>
</div>
<div id="result_status_super" style="display: none;">
<b># super-nodes/segmenten behandeld</b>
</div>
<div id="result_status_waytype" style="display: none;">
<b># wegtype segmenten behandeld</b>
</div>
<div id="result_status_highway" style="display: none;">
<b># segmenten behandeld</b>
</div>
<div id="result_status_transport" style="display: none;">
<b># segmenten behandeld</b>
</div>
<div id="result_status_barrier" style="display: none;">
<b># knooppunten behandeld</b>
</div>
<div id="result_status_turns" style="display: none;">
<b># afslagbeperkingen behandeld</b>
</div>
<div id="result_status_limit" style="display: none;">
<b>Processed # limit changes</b>
</div>
<div id="result_status_property" style="display: none;">
<b># segmenten behandeld</b>
</div>
<div id="result_status_errorlogs" style="display: none;">
<b># foutmeldingen behandeld</b>
</div>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_junctions_show" onclick="hideshow_show('junctions');" class="hideshow_show">+</span>
<span id="hideshow_junctions_hide" onclick="hideshow_hide('junctions');" class="hideshow_hide">-</span>
<input type="button" id="junctions" onclick="displayData('junctions');" value="Toon Splitsingen">
<div id="hideshow_junctions_div" style="display: none;">
Ieder knooppunt dat doodloopt, een kruispunt vormt van wegen van een verschillend type (of met verschillende eigenschappen) of kruispunten waar meer dan twee segmenten samenkomen worden met kleur gecodeerd.
<br>
<table>
<tr><td><img src="icons/ball-1.png" alt="1" ><td>Slechts één weg - een doodlopend punt
<tr><td><img src="icons/ball-2.png" alt="2" ><td>kruispunt van twee wegen van een verschillend type
<tr><td><img src="icons/ball-3.png" alt="3" ><td>kruispunt van drie wegen
<tr><td><img src="icons/ball-4.png" alt="4" ><td>kruispunt van vier wegen
<tr><td><img src="icons/ball-5.png" alt="5" ><td>kruispunt van vijf wegen
<tr><td><img src="icons/ball-6.png" alt="6" ><td>kruispunt van zes wegen
<tr><td><img src="icons/ball-7.png" alt="7+"><td>kruispunt van zeven (of meer) wegen
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_super_show" onclick="hideshow_show('super');" class="hideshow_show">+</span>
<span id="hideshow_super_hide" onclick="hideshow_hide('super');" class="hideshow_hide">-</span>
<input type="button" id="super" onclick="displayData('super');" value="Toon Super Segmenten">
<div id="hideshow_super_div" style="display: none;">
Ieder super-knooppunt en de daarmee verbonden supersegmenten worden getoond (zie de pagina met beschrijving van het algoritme)
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_waytype_show" onclick="hideshow_show('waytype');" class="hideshow_show">+</span>
<span id="hideshow_waytype_hide" onclick="hideshow_hide('waytype');" class="hideshow_hide">-</span>
<input type="button" id="waytype" onclick="displayData('waytype');" value="Toon wegtype Segmenten">
<div id="hideshow_waytype_div" style="display: none;">
Elk wegsegment van een speciaal type (enkele richting, fiets-beide-richtingen en rotonde) wordt getoond. Voor segmenten met enkele richting toont een gekleurde driehoek de toegestane richting. De kleur van de driehoek hangt af van de 'bearing' van het wegsegment
<form name="waytypes" id="waytypes" action="#" method="get" onsubmit="return false;">
<table>
<tr><td>Segment met enkele richting: <td><input name="waytype" type="radio" value="oneway" onchange="displayData('waytype');" checked>
<tr><td>Fiets-in-beide-richtingen segment:<td><input name="waytype" type="radio" value="cyclebothways" onchange="displayData('waytype');">
<tr><td>Rotondesegment: <td><input name="waytype" type="radio" value="roundabout" onchange="displayData('waytype');">
</table>
</form>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_highway_show" onclick="hideshow_show('highway');" class="hideshow_show">+</span>
<span id="hideshow_highway_hide" onclick="hideshow_hide('highway');" class="hideshow_hide">-</span>
<input type="button" id="highway" onclick="displayData('highway');" value="Toon Wegsegmenten">
<div id="hideshow_highway_div" style="display: none;">
Ieder segment van het gekozen type weg wordt getekend
<form name="highways" id="highways" action="#" method="get" onsubmit="return false;">
<table>
<tr><td>Autosnelweg: <td><input name="highway" type="radio" value="motorway" onchange="displayData('highway');">
<tr><td>Autoweg: <td><input name="highway" type="radio" value="trunk" onchange="displayData('highway');">
<tr><td>Primair: <td><input name="highway" type="radio" value="primary" onchange="displayData('highway');" checked>
<tr><td>Secundair: <td><input name="highway" type="radio" value="secondary" onchange="displayData('highway');">
<tr><td>Tertiair: <td><input name="highway" type="radio" value="tertiary" onchange="displayData('highway');">
<tr><td>Niet geclassificeerd:<td><input name="highway" type="radio" value="unclassified" onchange="displayData('highway');">
<tr><td>Woongebied: <td><input name="highway" type="radio" value="residential" onchange="displayData('highway');">
<tr><td>Toegangsweg: <td><input name="highway" type="radio" value="service" onchange="displayData('highway');">
<tr><td>Veldweg: <td><input name="highway" type="radio" value="track" onchange="displayData('highway');">
<tr><td>Fietspad: <td><input name="highway" type="radio" value="cycleway" onchange="displayData('highway');">
<tr><td>Pad: <td><input name="highway" type="radio" value="path" onchange="displayData('highway');">
<tr><td>Trap: <td><input name="highway" type="radio" value="steps" onchange="displayData('highway');">
<tr><td>Ferry: <td><input name="highway" type="radio" value="ferry" onchange="displayData('highway');">
</table>
</form>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_transport_show" onclick="hideshow_show('transport');" class="hideshow_show">+</span>
<span id="hideshow_transport_hide" onclick="hideshow_hide('transport');" class="hideshow_hide">-</span>
<input type="button" id="transport" onclick="displayData('transport');" value="Toon Transportsegmenten">
<div id="hideshow_transport_div" style="display: none;">
Ieder segment waar het gekozen type van transport is toegelaten, wordt getekend
<form name="transports" id="transports" action="#" method="get" onsubmit="return false;">
<table>
<tr><td>Te voet: <td><input name="transport" type="radio" value="foot" onchange="displayData('transport');">
<tr><td>Paard: <td><input name="transport" type="radio" value="horse" onchange="displayData('transport');">
<tr><td>Rolstoel:<td><input name="transport" type="radio" value="wheelchair" onchange="displayData('transport');">
<tr><td>Fiets: <td><input name="transport" type="radio" value="bicycle" onchange="displayData('transport');">
<tr><td>Brommer: <td><input name="transport" type="radio" value="moped" onchange="displayData('transport');">
<tr><td>Motorfiets:<td><input name="transport" type="radio" value="motorcycle" onchange="displayData('transport');">
<tr><td>Auto: <td><input name="transport" type="radio" value="motorcar" onchange="displayData('transport');" checked>
<tr><td>Goederen: <td><input name="transport" type="radio" value="goods" onchange="displayData('transport');">
<tr><td>Zwaar transport: <td><input name="transport" type="radio" value="hgv" onchange="displayData('transport');">
<tr><td>Publiek transport: <td><input name="transport" type="radio" value="psv" onchange="displayData('transport');">
</table>
</form>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_barrier_show" onclick="hideshow_show('barrier');" class="hideshow_show">+</span>
<span id="hideshow_barrier_hide" onclick="hideshow_hide('barrier');" class="hideshow_hide">-</span>
<input type="button" id="barrier" onclick="displayData('barrier');" value="Toon barrière-knooppunten">
<div id="hideshow_barrier_div" style="display: none;">
Ieder barrière die het gekozen type van transport blokkeert wordt getekend
<form name="barriers" id="barriers" action="#" method="get" onsubmit="return false;">
<table>
<tr><td>Te voet: <td><input name="barrier" type="radio" value="foot" onchange="displayData('barrier');">
<tr><td>Paard: <td><input name="barrier" type="radio" value="horse" onchange="displayData('barrier');">
<tr><td>Rolstoel:<td><input name="barrier" type="radio" value="wheelchair" onchange="displayData('barrier');">
<tr><td>Fiets: <td><input name="barrier" type="radio" value="bicycle" onchange="displayData('barrier');">
<tr><td>Brommer: <td><input name="barrier" type="radio" value="moped" onchange="displayData('barrier');">
<tr><td>Motorfiets:<td><input name="barrier" type="radio" value="motorcycle" onchange="displayData('barrier');">
<tr><td>Auto: <td><input name="barrier" type="radio" value="motorcar" onchange="displayData('barrier');" checked>
<tr><td>Goederen: <td><input name="barrier" type="radio" value="goods" onchange="displayData('barrier');">
<tr><td>Zwaar transport: <td><input name="barrier" type="radio" value="hgv" onchange="displayData('barrier');">
<tr><td>Publiek transport: <td><input name="barrier" type="radio" value="psv" onchange="displayData('barrier');">
</table>
</form>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_turns_show" onclick="hideshow_show('turns');" class="hideshow_show">+</span>
<span id="hideshow_turns_hide" onclick="hideshow_hide('turns');" class="hideshow_hide">-</span>
<input type="button" id="turns" onclick="displayData('turns');" value="Toon afslagbeperkingen">
<div id="hideshow_turns_div" style="display: none;">
Iedere afslagbeperking wordt getoond met een lijn die aanduidt welke afslag verboden is
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_speed_show" onclick="hideshow_show('speed');" class="hideshow_show">+</span>
<span id="hideshow_speed_hide" onclick="hideshow_hide('speed');" class="hideshow_hide">-</span>
<input type="button" id="speed" onclick="displayData('speed');" value="Toon snelheidslimieten">
<div id="hideshow_speed_div" style="display: none;">
Ieder knooppunt dat segmenten verbindt met verschillende snelheidslimieten wordt getoond, samen met de snelheidslimiet op de relevante segmenten
<br>
<table>
<tr><td><img src="icons/ball-1.png" alt="." ><td>Verandering van de snelheidslimiet
<tr><td><img src="icons/limit-no.png" alt="()" ><td>Geen snelheidslimiet bekend
<tr><td><img src="icons/limit-80.png" alt="(80)"><td>Snelheidslimiet 80 km/h
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_weight_show" onclick="hideshow_show('weight');" class="hideshow_show">+</span>
<span id="hideshow_weight_hide" onclick="hideshow_hide('weight');" class="hideshow_hide">-</span>
<input type="button" id="weight" onclick="displayData('weight');" value="Toon gewichtslimieten">
<div id="hideshow_weight_div" style="display: none;">
Ieder knooppunt dat segmenten verbindt met verschillende gewichtslimieten wordt getoond, samen met de gewichtslimieten van de relevante segmenten
<br>
<table>
<tr><td><img src="icons/ball-1.png" alt="." ><td>Verandering van de snelheidslimiet
<tr><td><img src="icons/limit-no.png" alt="()" ><td>Geen snelheidslimiet bekend
<tr><td><img src="icons/limit-8.0.png" alt="(8.0)"><td>8.0 ton gewichtslimiet
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_height_show" onclick="hideshow_show('height');" class="hideshow_show">+</span>
<span id="hideshow_height_hide" onclick="hideshow_hide('height');" class="hideshow_hide">-</span>
<input type="button" id="height" onclick="displayData('height');" value="Toon hoogtelimieten">
<div id="hideshow_height_div" style="display: none;">
Ieder knooppunt dat segmenten verbindt met verschillende hoogtelimieten wordt getoond, samen met de hoogtelimieten van de relevante segmenten
<br>
<table>
<tr><td><img src="icons/ball-1.png" alt="." ><td>Verandering van de snelheidslimiet
<tr><td><img src="icons/limit-no.png" alt="()" ><td>Geen snelheidslimiet bekend
<tr><td><img src="icons/limit-4.0.png" alt="(4.0)"><td>4.0 m hoogtelimiet
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_width_show" onclick="hideshow_show('width');" class="hideshow_show">+</span>
<span id="hideshow_width_hide" onclick="hideshow_hide('width');" class="hideshow_hide">-</span>
<input type="button" id="width" onclick="displayData('width');" value="Toon breedtelimieten">
<div id="hideshow_width_div" style="display: none;">
Ieder knooppunt dat segmenten verbindt met verschillende breedtelimieten wordt getoond, samen met de breedtelimieten van de relevante segmenten
<br>
<table>
<tr><td><img src="icons/ball-1.png" alt="." ><td>Verandering van de snelheidslimiet
<tr><td><img src="icons/limit-no.png" alt="()" ><td>Geen snelheidslimiet bekend
<tr><td><img src="icons/limit-3.0.png" alt="(3.0)"><td>3.0 m breedtelimiet
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_length_show" onclick="hideshow_show('length');" class="hideshow_show">+</span>
<span id="hideshow_length_hide" onclick="hideshow_hide('length');" class="hideshow_hide">-</span>
<input type="button" id="length" onclick="displayData('length');" value="Toon Lengtelimieten">
<div id="hideshow_length_div" style="display: none;">
Ieder knooppunt dat segmenten verbindt met verschillende lengtelimieten wordt getoond, samen met de lengtelimieten van de relevante segmenten
<br>
<table>
<tr><td><img src="icons/ball-1.png" alt="." ><td>Verandering van de snelheidslimiet
<tr><td><img src="icons/limit-no.png" alt="()" ><td>Geen snelheidslimiet bekend
<tr><td><img src="icons/limit-9.0.png" alt="(9.0)"><td>9.0 m lengtelimiet
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_property_show" onclick="hideshow_show('property');" class="hideshow_show">+</span>
<span id="hideshow_property_hide" onclick="hideshow_hide('property');" class="hideshow_hide">-</span>
<input type="button" id="property" onclick="displayData('property');" value="Toon Wegeigenschappen">
<div id="hideshow_property_div" style="display: none;">
Ieder segment van de wegen met een bepaalde eigenschap wordt getekend
<form name="properties" id="properties" action="#" method="get" onsubmit="return false;">
<table>
<tr><td>Verhard: <td><input name="property" type="radio" value="paved" onchange="displayData('property');" checked>
<tr><td>Meerdere Rijstroken: <td><input name="property" type="radio" value="multilane" onchange="displayData('property');">
<tr><td>Brug: <td><input name="property" type="radio" value="bridge" onchange="displayData('property');">
<tr><td>Tunnel: <td><input name="property" type="radio" value="tunnel" onchange="displayData('property');">
<tr><td>Wandelroute: <td><input name="property" type="radio" value="footroute" onchange="displayData('property');">
<tr><td>Fietsroute: <td><input name="property" type="radio" value="bicycleroute" onchange="displayData('property');">
</table>
</form>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_errorlogs_show" onclick="hideshow_show('errorlogs');" class="hideshow_show">+</span>
<span id="hideshow_errorlogs_hide" onclick="hideshow_hide('errorlogs');" class="hideshow_hide">-</span>
<input type="button" id="errorlogs" onclick="displayData('errorlogs');" value="Toon Foutmeldingen">
<div id="hideshow_errorlogs_div" style="display: none;">
Routino ontdekte mogelijke problemen bij het verwerken van de invoergegevens
</div>
</div>

<div class="hideshow_box">
<input type="button" id="clear" onclick="displayData('');" value="Wis gegevens">
</div>

<div class="hideshow_box">
<span class="hideshow_title">Links</span>
<a id="permalink_url" href="visualiser.html">Link naar dit kaartbeeld</a>
<br>
<a id="edit_url" target="edit" style="display: none;">Bewerk deze OSM gegevens</a>
</div>

<div class="hideshow_box">
<span id="hideshow_help_options_show" onclick="hideshow_show('help_options');" class="hideshow_hide">+</span>
<span id="hideshow_help_options_hide" onclick="hideshow_hide('help_options');" class="hideshow_show">-</span>
<span class="hideshow_title">Help</span>
<div id="hideshow_help_options_div">
<div class="scrollable">
<b>Snelle Start</b>
<br>
Zoom in op een gebied en kies één van de knoppen om dat type gegevens weer te geven
<br>
Meer gegevensopties kunnen getoond worden door de details achter iedere knop op te vragen
<p>
<b>Mislukte Visualisatie</b>
<br> 
Als het gekozen gebied te groot is (hangt af van het type gegevens) dan volgt er een bericht "Failed to get visualiser data" - zoom in en probeer opnieuw.
</div>
</div>
</div>
</div>

<div class="tab_content" id="tab_router_div" style="display: none;">
<div class="hideshow_box">
<span class="hideshow_title">Routino Router</span>
Om een route te berekenen op de kaart, volg de link hieronder
<br>
<a id="router_url" href="router.html" target="router">Link naar dit kaartbeeld</a>
</div>
</div>

<div class="tab_content" id="tab_data_div" style="display: none;">
<div class="hideshow_box">
<span class="hideshow_title">Routino statistieken</span>
<div id="statistics_data"></div>
<a id="statistics_link" href="statistics.cgi" onclick="displayStatistics();return(false);">Toon statistieken</a>
</div>
</div>

</div>

<!-- Right hand side of window - map -->

<div class="right_panel">
<div class="map" id="map">
<noscript>
<p>
Javascript is <em>noodzakelijk</em> om deze webpagina te gebruiken omwille van de interactieve kaart
</noscript>
</div>
<div class="attribution">
Router: <a href="http://www.routino.org/" target="routino">Routino</a>
|
Geo Data: <span id="attribution_data"></span>
|
Tiles: <span id="attribution_tile"></span>
</div>
</div>

</body>

</html>
