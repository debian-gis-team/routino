<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="openstreetmap routing route planner">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=no">

<title>Routino : Routeplanner voor OpenStreetMap gegevens</title>

<!--
Routino router web page.

Part of the Routino routing software.

This file Copyright 2008-2018 Andrew M. Bishop

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
-->

<!-- Page elements -->
<script src="page-elements.js" type="text/javascript"></script>
<link href="page-elements.css" type="text/css" rel="stylesheet">

<!-- Router and visualiser shared features -->
<link href="maplayout.css" type="text/css" rel="stylesheet">

<!-- Router specific features -->
<script src="profiles.js" type="text/javascript"></script>
<link href="router.css" type="text/css" rel="stylesheet">

<!-- Map parameters -->
<script src="mapprops.js" type="text/javascript"></script>

<!-- Map loader -->
<script src="maploader.js" type="text/javascript"></script>

</head>
<body onload="map_load('html_init();map_init();form_init();');">

<!-- Left hand side of window - data panel -->

<div class="left_panel">

<div class="tab_box">
<span id="tab_options" onclick="tab_select('options');" class="tab_selected" title="Kies opties voor routering">Opties</span>
<span id="tab_results" onclick="tab_select('results');" class="tab_unselected" title="Bekijk routeringsresultaten">Resultaten</span>
<span id="tab_data" onclick="tab_select('data');" class="tab_unselected" title="Bekijk de informatie in de database">Gegevens</span>
</div>

<div class="tab_content" id="tab_options_div">

<form name="form" id="form" action="#" method="get" onsubmit="return false;">
<div class="hideshow_box">
<span class="hideshow_title">Routino OpenStreetMap routering</span>
Deze webpagina laat je een route plannen op basis van gegevens van OpenStreetMap.
Selecteer start- and eindpunten (klik op het markericoon hieronder), kies routevoorkeuren en vind een route
<div class="center">
<a target="other" href="http://www.routino.org/">Routino Website</a>
|
<a target="other" href="documentation/">Documentatie</a>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_language_show" onclick="hideshow_show('language');" class="hideshow_show">+</span>
<span id="hideshow_language_hide" onclick="hideshow_hide('language');" class="hideshow_hide">-</span>
<span class="hideshow_title">Taal</span>

<div id="hideshow_language_div" style="display: none;">
<table>
<tr>
<td><a id="lang_en_url" href="router.html.en" title="English language webpage">English</a>
<td>(EN)
<td><input name="language" type="radio" value="en" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_cs_url" href="router.html.cs" title="Stránka v češtině">Česky</a>
<td>(CS)
<td><input name="language" type="radio" value="cs" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_de_url" href="router.html.de" title="Deutsche Webseite">Deutsch</a>
<td>(DE)
<td><input name="language" type="radio" value="de" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_es_url" href="router.html.es" title="Español webpage">Español</a>
<td>(ES)
<td><input name="language" type="radio" value="es" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_fi_url" href="router.html.fi" title="Suomi webpage">Suomi</a>
<td>(FI)
<td><input name="language" type="radio" value="fi" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_fr_url" href="router.html.fr" title="Page web en français">Français</a>
<td>(FR)
<td><input name="language" type="radio" value="fr" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_hu_url" href="router.html.hu" title="Magyar weboldal">Magyar</a>
<td>(HU)
<td><input name="language" type="radio" value="hu" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_it_url" href="router.html.it" title="Italiano webpage">Italiano</a>
<td>(IT)
<td><input name="language" type="radio" value="it" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_nl_url" href="router.html.nl" title="Nederlandse web pagina">Nederlands</a>
<td>(NL)
<td><input name="language" type="radio" value="nl" onchange="formSetLanguage();" checked>
<tr>
<td><a id="lang_pl_url" href="router.html.pl" title="Polski webpage">Polski</a>
<td>(PL)
<td><input name="language" type="radio" value="pl" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_ru_url" href="router.html.ru" title="Страница на русском языке">Русский</a>
<td>(RU)
<td><input name="language" type="radio" value="ru" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_sk_url" href="router.html.sk" title="Stránka v slovenčine">Slovenčina</a>
<td>(SK)
<td><input name="language" type="radio" value="sk" onchange="formSetLanguage();" >
</table>
<a target="translation" href="http://www.routino.org/translations/">Routino Translations</a>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_waypoint_show" onclick="hideshow_show('waypoint');" class="hideshow_hide">+</span>
<span id="hideshow_waypoint_hide" onclick="hideshow_hide('waypoint');" class="hideshow_show">-</span>
<span class="hideshow_title">Routepunten</span>
<div id="hideshow_waypoint_div">
<div id="waypoints">
<div id="waypointXXX" class="waypoint" style="display: none;">
<img id="iconXXX" class="waypoint-icon" src="icons/marker-XXX-grey.png" title="Routepunt XXX Positie" alt="Waypoint XXX" onmouseup="markerToggleMap(XXX);" draggable="true">
<span id="coordsXXX" style="display: none;">
<input name="lonXXX" type="text" size="6" title="Routepunt XXX Lengtegraad" onchange="formSetCoords(XXX);">E
<input name="latXXX" type="text" size="7" title="Routepunt XXX Breedtegraad" onchange="formSetCoords(XXX);">N
</span>
<span id="searchXXX">
<input name="searchXXX" type="text" size="18" title="Routepunt XXX Plaats"> <!-- uses Javascript event for triggering -->
</span>
<div class="waypoint-buttons" style="display: inline-block;">
<img alt="?" src="icons/waypoint-search.png" title="Zoek plaats" onmousedown="markerSearch(XXX);" >
<img alt="G" src="icons/waypoint-locate.png" title="Vind huidige plaats" onmousedown="markerLocate(XXX);" >
<img alt="O" src="icons/waypoint-recentre.png" title="Centreer kaart op dit routepunt" onmousedown="markerRecentre(XXX);">
<img alt="^" src="icons/waypoint-up.png" title="Beweeg dit routepunt naar boven" onmousedown="markerMoveUp(XXX);" >
<img alt="+" src="icons/waypoint-add.png" title="Voeg hierna routepunt toe" onmousedown="markerAddAfter(XXX);">
<br>
<img alt="#" src="icons/waypoint-coords.png" title="Coördinaten van de plaats" onmousedown="markerCoords(XXX);" >
<img alt="~" src="icons/waypoint-home.png" title="Toggle als basisplaats ('thuis')" onmousedown="markerHome(XXX);" >
<img alt="o" src="icons/waypoint-centre.png" title="Centreer dit routepunt op de kaart" onmousedown="markerCentre(XXX);" >
<img alt="v" src="icons/waypoint-down.png" title="Beweeg dit routepunt naar beneden" onmousedown="markerMoveDown(XXX);">
<img alt="-" src="icons/waypoint-remove.png" title="Verwijder dit routepunt" onmousedown="markerRemove(XXX);" >
</div>
<div id="searchresultsXXX" style="display: none;">
</div>
</div>
</div>
<div id="waypoints-buttons">
<table>
<tr><td>Sluit de lus: <td><input type="checkbox" name="loop" onchange="formSetLoopReverse('loop' );">
<tr><td>Keer volgorde om:<td><input type="checkbox" name="reverse" onchange="formSetLoopReverse('reverse');">
</table>
<div class="waypoint-buttons" style="display: inline-block;">
<img src="icons/waypoint-loop.png" title="Voeg routepunt toe om lus te maken" onmousedown="markersLoop();">
<img src="icons/waypoint-reverse.png" title="Keer volgorde van de routepunten om" onmousedown="markersReverse();">
</div>
</div>
</div>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Vind</span>
<input type="button" title="Zoek de kortste route" id="shortest1" value="Kortste Route" onclick="findRoute('shortest');" disabled="disabled">
<input type="button" title="Zoek de snelste route" id="quickest1" value="Snelste Route" onclick="findRoute('quickest');" disabled="disabled">
</div>

<div class="hideshow_box">
<span id="hideshow_transport_show" onclick="hideshow_show('transport');" class="hideshow_hide">+</span>
<span id="hideshow_transport_hide" onclick="hideshow_hide('transport');" class="hideshow_show">-</span>
<span class="hideshow_title">Transporttype</span>
<div id="hideshow_transport_div">
<table>
<tr><td>Te voet: <td><input name="transport" type="radio" value="foot" onchange="formSetTransport('foot' );">
<tr><td>Paard: <td><input name="transport" type="radio" value="horse" onchange="formSetTransport('horse' );">
<tr><td>Rolstoel:<td><input name="transport" type="radio" value="wheelchair" onchange="formSetTransport('wheelchair');">
<tr><td>Fiets: <td><input name="transport" type="radio" value="bicycle" onchange="formSetTransport('bicycle' );">
<tr><td>Brommer: <td><input name="transport" type="radio" value="moped" onchange="formSetTransport('moped' );">
<tr><td>Motorfiets:<td><input name="transport" type="radio" value="motorcycle" onchange="formSetTransport('motorcycle');">
<tr><td>Auto: <td><input name="transport" type="radio" value="motorcar" onchange="formSetTransport('motorcar' );">
<tr><td>Goederen: <td><input name="transport" type="radio" value="goods" onchange="formSetTransport('goods' );">
<tr><td>Zwaar transport: <td><input name="transport" type="radio" value="hgv" onchange="formSetTransport('hgv' );">
<tr><td>Publiek transport: <td><input name="transport" type="radio" value="psv" onchange="formSetTransport('psv' );">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_highway_show" onclick="hideshow_show('highway');" class="hideshow_show">+</span>
<span id="hideshow_highway_hide" onclick="hideshow_hide('highway');" class="hideshow_hide">-</span>
<span class="hideshow_title">Voorkeur Wegtype</span>
<div id="hideshow_highway_div" style="display: none;">
<table>
<tr><td>Autosnelweg: <td><input name="highway-motorway" type="text" size="3" onchange="formSetHighway('motorway' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('motorway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('motorway' ,'+');">
<tr><td>Autoweg: <td><input name="highway-trunk" type="text" size="3" onchange="formSetHighway('trunk' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('trunk' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('trunk' ,'+');">
<tr><td>Primair: <td><input name="highway-primary" type="text" size="3" onchange="formSetHighway('primary' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('primary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('primary' ,'+');">
<tr><td>Secundair: <td><input name="highway-secondary" type="text" size="3" onchange="formSetHighway('secondary' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('secondary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('secondary' ,'+');">
<tr><td>Tertiair: <td><input name="highway-tertiary" type="text" size="3" onchange="formSetHighway('tertiary' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('tertiary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('tertiary' ,'+');">
<tr><td>Niet geclassificeerd:<td><input name="highway-unclassified" type="text" size="3" onchange="formSetHighway('unclassified','=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('unclassified','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('unclassified','+');">
<tr><td>Woongebied: <td><input name="highway-residential" type="text" size="3" onchange="formSetHighway('residential' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('residential' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('residential' ,'+');">
<tr><td>Toegangsweg: <td><input name="highway-service" type="text" size="3" onchange="formSetHighway('service' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('service' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('service' ,'+');">
<tr><td>Veldweg: <td><input name="highway-track" type="text" size="3" onchange="formSetHighway('track' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('track' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('track' ,'+');">
<tr><td>Fietspad: <td><input name="highway-cycleway" type="text" size="3" onchange="formSetHighway('cycleway' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('cycleway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('cycleway' ,'+');">
<tr><td>Pad: <td><input name="highway-path" type="text" size="3" onchange="formSetHighway('path' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('path' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('path' ,'+');">
<tr><td>Trap: <td><input name="highway-steps" type="text" size="3" onchange="formSetHighway('steps' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('steps' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('steps' ,'+');">
<tr><td>Ferry: <td><input name="highway-ferry" type="text" size="3" onchange="formSetHighway('ferry' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('ferry' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('ferry' ,'+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_speed_show" onclick="hideshow_show('speed');" class="hideshow_show">+</span>
<span id="hideshow_speed_hide" onclick="hideshow_hide('speed');" class="hideshow_hide">-</span>
<span class="hideshow_title">Snelheidslimieten</span>
<div id="hideshow_speed_div" style="display: none;">
<table>
<tr><td>Autosnelweg: <td><input name="speed-motorway" type="text" size="3" onchange="formSetSpeed('motorway' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('motorway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('motorway' ,'+');">
<tr><td>Autoweg: <td><input name="speed-trunk" type="text" size="3" onchange="formSetSpeed('trunk' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('trunk' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('trunk' ,'+');">
<tr><td>Primair: <td><input name="speed-primary" type="text" size="3" onchange="formSetSpeed('primary' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('primary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('primary' ,'+');">
<tr><td>Secundair: <td><input name="speed-secondary" type="text" size="3" onchange="formSetSpeed('secondary' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('secondary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('secondary' ,'+');">
<tr><td>Tertiair: <td><input name="speed-tertiary" type="text" size="3" onchange="formSetSpeed('tertiary' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('tertiary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('tertiary' ,'+');">
<tr><td>Niet geclassificeerd:<td><input name="speed-unclassified" type="text" size="3" onchange="formSetSpeed('unclassified','=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('unclassified','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('unclassified','+');">
<tr><td>Woongebied: <td><input name="speed-residential" type="text" size="3" onchange="formSetSpeed('residential' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('residential' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('residential' ,'+');">
<tr><td>Toegangsweg: <td><input name="speed-service" type="text" size="3" onchange="formSetSpeed('service' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('service' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('service' ,'+');">
<tr><td>Veldweg: <td><input name="speed-track" type="text" size="3" onchange="formSetSpeed('track' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('track' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('track' ,'+');">
<tr><td>Fietspad: <td><input name="speed-cycleway" type="text" size="3" onchange="formSetSpeed('cycleway' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('cycleway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('cycleway' ,'+');">
<tr><td>Pad: <td><input name="speed-path" type="text" size="3" onchange="formSetSpeed('path' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('path' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('path' ,'+');">
<tr><td>Trap: <td><input name="speed-steps" type="text" size="3" onchange="formSetSpeed('steps' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('steps' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('steps' ,'+');">
<tr><td>Ferry: <td><input name="speed-ferry" type="text" size="3" onchange="formSetSpeed('ferry' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('ferry' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('ferry' ,'+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_property_show" onclick="hideshow_show('property');" class="hideshow_show">+</span>
<span id="hideshow_property_hide" onclick="hideshow_hide('property');" class="hideshow_hide">-</span>
<span class="hideshow_title">Voorkeur Eigenschappen</span>
<div id="hideshow_property_div" style="display: none;">
<table>
<tr><td>Verhard: <td><input name="property-paved" type="text" size="3" onchange="formSetProperty('paved' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('paved' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('paved' ,'+');">
<tr><td>Meerdere Rijstroken: <td><input name="property-multilane" type="text" size="3" onchange="formSetProperty('multilane' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('multilane' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('multilane' ,'+');">
<tr><td>Brug: <td><input name="property-bridge" type="text" size="3" onchange="formSetProperty('bridge' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('bridge' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('bridge' ,'+');">
<tr><td>Tunnel: <td><input name="property-tunnel" type="text" size="3" onchange="formSetProperty('tunnel' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('tunnel' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('tunnel' ,'+');">
<tr><td>Wandelroute:<td><input name="property-footroute" type="text" size="3" onchange="formSetProperty('footroute' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('footroute' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('footroute' ,'+');">
<tr><td>Fietsroute:<td><input name="property-bicycleroute" type="text" size="3" onchange="formSetProperty('bicycleroute','=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('bicycleroute','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('bicycleroute','+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_restriction_show" onclick="hideshow_show('restriction');" class="hideshow_show">+</span>
<span id="hideshow_restriction_hide" onclick="hideshow_hide('restriction');" class="hideshow_hide">-</span>
<span class="hideshow_title">Andere Beperkingen</span>
<div id="hideshow_restriction_div" style="display: none;">
<table>
<tr><td>Volg Eenrichtingsverkeer:<td><input name="restrict-oneway" type="checkbox" onchange="formSetRestriction('oneway');">
<tr><td>Respecteer verboden afslagen: <td><input name="restrict-turns" type="checkbox" onchange="formSetRestriction('turns' );">
</table>
<table>
<tr><td>Gewicht:<td><input name="restrict-weight" type="text" size="3" onchange="formSetRestriction('weight','=');"><td>tonnes<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('weight','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('weight','+');">
<tr><td>Hoogte:<td><input name="restrict-height" type="text" size="3" onchange="formSetRestriction('height','=');"><td>metres<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('height','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('height','+');">
<tr><td>Breedte: <td><input name="restrict-width" type="text" size="3" onchange="formSetRestriction('width' ,'=');"><td>metres<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('width' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('width' ,'+');">
<tr><td>Lengte:<td><input name="restrict-length" type="text" size="3" onchange="formSetRestriction('length','=');"><td>metres<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('length','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('length','+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Links</span>
<a id="permalink_url" href="router.html">Link naar dit kaartbeeld</a>
<br>
<a id="edit_url" target="edit" style="display: none;">Bewerk deze OSM gegevens</a>
</div>

<div class="hideshow_box">
<span id="hideshow_help_options_show" onclick="hideshow_show('help_options');" class="hideshow_hide">+</span>
<span id="hideshow_help_options_hide" onclick="hideshow_hide('help_options');" class="hideshow_show">-</span>
<span class="hideshow_title">Help</span>
<div id="hideshow_help_options_div">
<div class="scrollable">
<b>Snelle Start</b>
<br>
Klik op markericoontje (boven) om ze op de kaart te plaatsen (rechts).
Sleep ze vervolgens naar de gewenste positie.
Het is best om eerst naar straat niveau te zoomen op de kaart. Een atlternatief is om lengte- en breedtegraad in te voeren in de invoervelden hierboven.
<p>
Selecteer het transport type, toegestane wegtypes,
snelheidslimieten, wegeigenschappen en andere restricties uit de
opties.
Selecteer "Kortste" of "Snelste" om de route te berekenen en te tekenen op de kaar.
<p>
<b>Routepunten (Waypoints)</b>
<br>
Klik op het marker icoontje, nog eens klikken voor aan/uit.
Wanneer de route berekend wordt, zal ze zo nauwkeurig mogelijk (voor het gegeven transporttype) aansluiten bij deze punten, in de gegeven volgorde. 
<p>
<b>Transporttype</b>
<br>
Bij selectie van een transporttype wordt de berekende route beperkt tot segmenten waar dit transport toegelaten is, terwijl standaardwaarden worden gebruikt voor de andere parameters.
<p>
<b>Voorkeur Wegtype</b>
<br>
De voorkeur voor een bepaald type weg wordt uitgedrukt in een percentage. De gekozen routes proberen de voorkeurswegen te volgen.
Bijvoorbeeld wanneer u het Transport Type "Fiets" kiest, dan zal er
voor Autosnelweg 0% staan, en voor Fietspad 100%.
Wanneer u Autowegen, Nationale wegen wil vermijden of beperken bij
het maken van een fietsroute, kan u percentage naar beneden
aanpassen.
<p>
<b>Snelheidslimieten</b>
<br>
De snelheidslimieten worden afgeleid van het type weg. Het is
mogelijk dat er voor een bepaalde weg andere beperkingen gelden. In
dat geval worden die gekozen, tenminste als ze lager zijn dan de standaardwaarden.
<p>
<b>Wegeigenschappen</b>
<br>
De voorkeur voor een eigenschap wordt gegeven als een percentage. De berekende route volgt bij voorkeur wegen met de gekozen eigenschap.
Wanneer u bijvoorbeeld verharde wegen een voorkeur van 75% geeft, dan zal een onverharde weg automatisch een voorkeur van 25 % krijgen. Een route over verharde weg die driemaal langer is dan een route over onverharde weg, zal toch nog de voorkeur krijgen bij de berekening.
<p>
<b>Andere Beperkingen</b>
<br>
Deze zullen toelaten dat er een route berekend wordt die rekening
houdt met gewicht, hoogte, breedte of lengte.
Het is ook mogelijk geen rekening te houden met eenrichtingsverkeer
(bijvoorbeeld als voetganger)
</div>
</div>
</div>
</form>
</div>


<div class="tab_content" id="tab_results_div" style="display: none;">

<div class="hideshow_box">
<span class="hideshow_title">Status</span>
<div id="result_status">
<div id="result_status_not_run">
<b><i>Router niet in gebruik</i></b>
</div>
<div id="result_status_running" style="display: none;">
<b>Router werkt...</b>
</div>
<div id="result_status_complete" style="display: none;">
<b>Routering voltooid</b>
<br>
<a id="router_log_complete" target="router_log" href="#">Bekijk Details</a>
</div>
<div id="result_status_error" style="display: none;">
<b>Routeringsfout</b>
<br>
<a id="router_log_error" target="router_log" href="#">Bekijk Details</a>
</div>
<div id="result_status_failed" style="display: none;">
<b>Router werkt niet</b>
</div>
</div>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Vind</span>
<input type="button" title="Zoek de kortste route" id="shortest2" value="Kortste Route" onclick="findRoute('shortest');" disabled="disabled">
<input type="button" title="Zoek de snelste route" id="quickest2" value="Snelste Route" onclick="findRoute('quickest');" disabled="disabled">
</div>

<div class="hideshow_box">
<span id="hideshow_shortest_show" onclick="hideshow_show('shortest');" class="hideshow_show">+</span>
<span id="hideshow_shortest_hide" onclick="hideshow_hide('shortest');" class="hideshow_hide">-</span>
<span class="hideshow_title">Kortste Route</span>
<div id="shortest_status">
<div id="shortest_status_no_info">
<b><i>Geen Informatie</i></b>
</div>
<div id="shortest_status_info" style="display: none;">
</div>
</div>
<div id="hideshow_shortest_div" style="display: none;">
<div id="shortest_links" style="display: none;">
<table>
<tr><td>HTML aanwijzingen: <td><a id="shortest_html" target="shortest_html" href="#">Open Popup</a>
<tr><td>GPX trackbestand: <td><a id="shortest_gpx_track" target="shortest_gpx_track" href="#">Open Popup</a>
<tr><td>GPX routebestand: <td><a id="shortest_gpx_route" target="shortest_gpx_route" href="#">Open Popup</a>
<tr><td>Volledig tekstbestand: <td><a id="shortest_text_all" target="shortest_text_all" href="#">Open Popup</a>
<tr><td>Tekstbestand: <td><a id="shortest_text" target="shortest_text" href="#">Open Popup</a>
</table>
<hr>
</div>
<div id="shortest_route">
</div>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_quickest_show" onclick="hideshow_show('quickest');" class="hideshow_show">+</span>
<span id="hideshow_quickest_hide" onclick="hideshow_hide('quickest');" class="hideshow_hide">-</span>
<span class="hideshow_title">Snelste Route</span>
<div id="quickest_status">
<div id="quickest_status_no_info">
<b><i>Geen Informatie</i></b>
</div>
<div id="quickest_status_info" style="display: none;">
</div>
</div>
<div id="hideshow_quickest_div" style="display: none;">
<div id="quickest_links" style="display: none;">
<table>
<tr><td>HTML aanwijzingen: <td><a id="quickest_html" target="quickest_html" href="#">Open Popup</a>
<tr><td>GPX trackbestand: <td><a id="quickest_gpx_track" target="quickest_gpx_track" href="#">Open Popup</a>
<tr><td>GPX routebestand: <td><a id="quickest_gpx_route" target="quickest_gpx_route" href="#">Open Popup</a>
<tr><td>Volledig tekstbestand: <td><a id="quickest_text_all" target="quickest_text_all" href="#">Open Popup</a>
<tr><td>Tekstbestand: <td><a id="quickest_text" target="quickest_text" href="#">Open Popup</a>
</table>
<hr>
</div>
<div id="quickest_route">
</div>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_help_route_show" onclick="hideshow_show('help_route');" class="hideshow_hide">+</span>
<span id="hideshow_help_route_hide" onclick="hideshow_hide('help_route');" class="hideshow_show">-</span>
<span class="hideshow_title">Help</span>
<div id="hideshow_help_route_div">
<div class="scrollable">
<b>Quick Start</b>
<br>
Na het berekenen van een route, kan u de route downloaden als GPX bestand, of als tekstbestand met routebeschrijving (samenvatting of gedetailleerde versie).
Door met de muis over de beschrijving te bewegen, ziet u die ook op de kaart gesitueerd.
<p>
<b>Problemen oplossen</b>
<br>
Als de router eindigt met een fout, dan is de meest waarschijnlijke
oorzaak, dat er geen route mogelijk is tussen de gekozen punten.
Het verplaatsen van de punten, of het aanpassen van weg-eigenschappen
of voertuigtype kan een oplossing bieden.
<p>
<b>Types Uitvoer</b>
<br>
<dl>
<dt>HTML instructies
<dd>Een beschrijving van de route, met de te nemen afslag aan iedere splitsing.
<dt>GPX track bestand
<dd>Dezelfde informatie die op de kaart wordt weergegeven. Met
coordinaten voor ieder knooppunt, en een lijn voor ieder segment.
<dt>GPX route bestand
<dd>Dezelfde informatie die is opgenomen in de tekst van de route,
met een knooppunt voor iedere belangrijke splitsing.
<dt>Volledig tekstbestand
<dd>Een lijst met alle knooppunten die de route aandoet, de afstand tussen deze knooppunten en de cumulatieve afstand voor iedere stap op de route.
<dt>Tekstbestand
<dd>Dezelfde informatie als wordt weergegeven in de tekst voor de route.
</dl>
</div>
</div>
</div>
</div>


<div class="tab_content" id="tab_data_div" style="display: none;">
<div class="hideshow_box">
<span class="hideshow_title">Routino statistieken</span>
<div id="statistics_data"></div>
<a id="statistics_link" href="statistics.cgi" onclick="displayStatistics();return(false);">Toon statistieken</a>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Routino Visualisering</span>
Om te kijken hoe Routino omgaat met de basisdata,
is er een tooltje dat de onderliggende data toont op verschillende manieren.
<br>
<a id="visualiser_url" href="visualiser.html" target="visualiser">Link naar dit kaartbeeld</a>
</div>
</div>

</div>

<!-- Right hand side of window - map -->

<div class="right_panel">
<div class="map" id="map">
<noscript>
<p>
Javascript is <em>noodzakelijk</em> om deze webpagina te gebruiken omwille van de interactieve kaart
</noscript>
</div>
<div class="attribution">
Router: <a href="http://www.routino.org/" target="routino">Routino</a>
|
Geo Data: <span id="attribution_data"></span>
|
Tiles: <span id="attribution_tile"></span>
</div>
</div>

</body>

</html>
